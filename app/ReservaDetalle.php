<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaDetalle extends Model
{
        protected $fillable = [
        'butaca', 'reserva_id',    ];



        public function reserva()
        {
            return $this->belongsTo(Reserva::class);
        }




}
