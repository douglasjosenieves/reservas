<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;



class UserController extends Controller
{

  public function index(Request $request)
  {

    $user = User::orderBy('id', 'desc')->paginate(15);
    $data['user'] = $user;
    return view('usuarios.index', $data);
  }



  public function create(Request $request)
  {

    return view('usuarios.create');
  }



  public function store(Request $request)
  {

    $this->validate($request, [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
    ]);


    $user = new  User();
    $user->name = $request->name;
    $user->email = $request->email;
    $user->password = bcrypt($request->password);
    $user->save();

    Session::flash('success','Se ha guardado el registro!');


    return redirect()->back();
  }




  public function update (Request $request, User $usuario)
  {

    $this->validate($request, [
      'name' => 'required|string|max:255',
      'email' => 'unique:users,email,'.$usuario->id

    ]);

    $usuario->name = $request->name;
    $usuario->email = $request->email;
    $usuario->update();

    Session::flash('warning','Se ha actualizado el registro!');


    return redirect()->back();
  }



  public function destroy (Request $request, User $usuario)
  {


    $usuario->delete();


    Session::flash('error','Se ha eliminado el registro!');
    return redirect()->back();

  }







}
