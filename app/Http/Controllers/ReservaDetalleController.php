<?php

namespace App\Http\Controllers;

use App\ReservaDetalle;
use Illuminate\Http\Request;

class ReservaDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReservaDetalle  $reservaDetalle
     * @return \Illuminate\Http\Response
     */
    public function show(ReservaDetalle $reservaDetalle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReservaDetalle  $reservaDetalle
     * @return \Illuminate\Http\Response
     */
    public function edit(ReservaDetalle $reservaDetalle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReservaDetalle  $reservaDetalle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReservaDetalle $reservaDetalle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReservaDetalle  $reservaDetalle
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReservaDetalle $reservaDetalle)
    {
        //
    }
}
