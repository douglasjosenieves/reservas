<?php

namespace App\Http\Controllers;

use App\Butaca;
use App\Reserva;
use App\ReservaDetalle;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class ReservaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $reservas = Reserva::orderBy('id', 'desc')->paginate(15);
        $data['reservas'] = $reservas;
        return view('reservas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function show(Reserva $reserva)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function edit(Reserva $reserva)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reserva $reserva)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reserva $reserva)
    {
       $reserva->delete();


       Session::flash('error','Se ha eliminado el registro!');
       return redirect()->back();
   }


   public function reservar(Request $request)
   {

      if (empty($request->select)) {

        Session::flash('error','Seleccione almenos una butaca...!');
        return redirect()->back()->withInput();;
    }

    $user = User::where('email', $request->email)->first();
    if ($user == NULL) {
        Session::flash('error','Este usuario no esta registrado...! Ir al menu Usuarios');

        return redirect()->back()->withInput();
    }

    $total_butacas = count($request->select);
    $monto = Config::get('app.precio') * $total_butacas ;
    $reserva = new Reserva;

    $reserva->monto = $monto;
    $reserva->user_id = $user->id;
    $reserva->save(); 

    foreach ($request->select as $key => $value) {

        Log::info('Reserva ID: '.$reserva->id. 'Butaca: '.$value);
        $rd = new ReservaDetalle();
        $rd->reserva_id = $reserva->id;
        $rd->butaca =  $value;
        $rd->save();


        $b = Butaca::where('code', $value)->first();
        $b->active = TRUE;
        $b->update();
    }

    Session::flash('success','Se ha reservado con exito!');

    return redirect()->back();


}


public function limpiar ($value='')
{


    Butaca::truncate();

    for ($i = 1; $i <= Config::get('app.filas'); $i++) { 

        for ($c = 1; $c <= Config::get('app.columnas'); $c++) { 

            $butaca = new Butaca;
            $butaca->code = $i.'-'.$c;
            $butaca->save();

        }


    }


    Session::flash('success','Se ha limpiado con exito!');

    return redirect()->back();

}

}
