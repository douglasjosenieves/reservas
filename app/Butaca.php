<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Butaca extends Model
{
    protected $fillable = [
        'code', 'active',    ];

}
