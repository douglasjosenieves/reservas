<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
        protected $fillable = [
        'monto', 'user_id',    ];

        

        public function detalles()
        {
            return $this->hasMany(ReservaDetalle::class, 'reserva_id', 'id');
        }


        public function user()
        {
            return $this->belongsTo(User::class);
        }
}
