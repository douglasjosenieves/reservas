<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'AppController@home');
Route::get('/callback', 'AppController@callback');
Route::resource('/reservas', 'ReservaController');
Route::resource('/usuarios', 'UserController');
Route::resource('/reservas', 'ReservaController');
Route::post('/reservar', 'ReservaController@reservar');
Route::post('/reservas-limpiar', 'ReservaController@limpiar');
 
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');