		<div class="row">
			@if ($errors->any())


				@foreach ($errors->all() as $error)
				<div class="alert alert-info" role="alert">
					<strong>Info!</strong> {{ $error }}.
				</div>


				@endforeach
 
        @endif
        @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
    </div>
