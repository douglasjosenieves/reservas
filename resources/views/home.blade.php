@extends('layout.default')
@section('content')
<style type="text/css">

.col {padding: 15px 25px;
	border-color: #333;
	border-style: dotted;
	cursor: pointer;

}
.ocupada {background-color: red; cursor: auto;}
.isActive {background-color: green;}


</style>
<div id="app">
	<div class="container"  >



		<div class="form" style="">
			{!! Form::open(['method' => 'POST', 'url' => '/reservar', 'class' => 'form-horizontal']) !!}


			<div class="box-body">


			</div>

			<div class="row">

				<div class="col-md-6"><div class="pull-rigth form-group{{ $errors->has('email') ? ' has-error' : '' }}">

					{!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Email eg. example@reserva.com']) !!}
					<small class="text-danger">{{ $errors->first('email') }}</small>
				</div>
			</div>
			<div class="col-md-6">	<div class="btn-group pull-rigth">

				{!! Form::submit("Reservar Ahora", ['class' => 'btn btn-success']) !!}
			</div>
		</div>
	</div>



	{!! Form::close() !!}

</div>
</div>
<hr>
<div class="container" >

	@include('partial.alert')
	<div class="row">

		@foreach ($butacas as  $key => $element)


		<div id="{{$element->code}}"  class="text-center col @include('partial.disponible')"  

			@if ($element->active == FALSE)
			@click="myFilter($event)"
			@endif


			data-code="{{$element->code}}">Butaca<br>{{$element->code}}</div>

			@if ($loop->iteration % 10 == 0)
		</div>
		<div class="row">
			@endif


			@endforeach
		</div>



	</div>

</div>
@endsection



@section('script')

<script type="text/javascript">

	var app = new Vue({
		el: '#app',
		data: {
			message: 'Hello Vue!',


		},

		methods: {
			myFilter: function(event){

				if (event) {

					if ($('#'+event.currentTarget.id).hasClass( "isActive" )) {

						$('#'+event.currentTarget.id).removeClass('isActive');


					} else {

						$('#'+event.currentTarget.id).addClass('isActive');



					}
					$(".box-body").html('');
					$(".isActive").each(function(){


						var input = $("<input/>", {
							type: "hidden",
							class: "form-control",
							id: "nameId",
							placeholder: "code",
							value: $(this).attr('data-code'),
							name: "select[]"
						});

						$(".box-body").append(input);

							//alert($(this).text())
						});




				}


			}
		} 
	})



</script>
@endsection