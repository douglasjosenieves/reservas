 @extends('layout.default')
 @section('title','Reservas')
 @php
 $modulo = 'reservas';
 @endphp
 @section('content')
 <style type="text/css">
 .negrita {font-weight: 490; }
 .detalle {color: #333; font-size: 12px}
 .detalle_padre {display: none;}
 .negrita_padre { cursor: pointer;}
</style>
<div class="container">

  <div class="row justify-content-center">
    <div class="col-md-10">

      <a href="{{ url('home') }}" class="btn btn-success">Nuevo</a>
      <hr> 

      <div class="card">


        <div class="card-header">@yield('title')</div>

        <div class="card-body">

          <div class="card-content">

           <br>
           <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" >
            <thead>
              <tr>
               <th>Id</th>

               <th>User</th>
               <th>Monto</th>      


               <th>Creado</th>
               <th>Acción</th>



             </tr>


             @foreach ($reservas as $element)

             <tbody class="negrita_padre">
               <tr >
                <td class="negrita">{{$element->id}} </td>
                <td class="negrita">{{@$element->user->name}} </td>
                <td class="negrita">{{$element->monto}} </td>             
                <td class="negrita">{{$element->created_at}}</td>
                <td class="negrita">@include('partial.action_delete')</td>

              </tr>
            </tbody>



            <tbody class="detalle_padre"> 
              @foreach ($element->detalles as $element2)
              <tr>
                <td class="detalle">Dt Id:{{$element2->id}} </td>
                <td class="detalle">Reserva Id:{{@$element2->reserva_id}} </td>
                <td class="detalle">Butaca N#: {{$element2->butaca}} </td>             
                <td class="detalle">{{$element2->created_at}}</td>
                <td class="detalle">Detalle</td>
              </tr>
              @endforeach
            </tbody>

            <!-- Classic Modal -->
            <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                      x
                    </button>

                  </div>
                  <div class="modal-body">



                    {!! Form::open(['method' => 'PUT', 'url' => 'usuarios/'.$element->id, 'class' => 'form-horizontal']) !!}







                  </div>


                  <div class="modal-footer">

                    {!! Form::submit('Actualizar', ['class' => 'btn btn-warning pull-right']) !!}

                    {!! Form::close() !!}

                  </div>
                </div>
              </div>
            </div>
            <!--  End Modal -->
            @endforeach



          </table>

          {{ $reservas->links() }}
        </div>

      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">

  $( ".negrita_padre" ).click(function() {


    $( ".detalle_padre" ).toggle( "slow" ).slow();


  });

</script>
@endsection