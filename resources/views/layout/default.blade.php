<!DOCTYPE html>
<!-- saved from url=(0060)https://getbootstrap.com/docs/4.1/examples/starter-template/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="https://getbootstrap.com/favicon.ico">
  <style type="text/css">
  main {
    margin-top: 80px;
  }
</style>

<title> {{Config('app.name')}}</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>

<body>

  <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="{{ url('/') }}">Salir</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{ url('/home') }}">+ Reservas <span class="sr-only">(current)</span></a>

        </li>  <li class="nav-item ">
          <a class="nav-link" href="{{ url('reservas') }}">Reservas Historico <span class="sr-only">(current)</span></a>
        </li>


      </li>  <li class="nav-item ">
        <a class="nav-link" href="{{ url('usuarios') }}">Usuarios <span class="sr-only">(current)</span></a>
      </li>


        </li>  <li class="nav-item ">
        <a class="nav-link" href="{{ url('logs') }}">Logs <span class="sr-only">(current)</span></a>
      </li>

    </li>  <li class="nav-item ">

      <a href="{{ url('reservas-limpiar/') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('limpiar-form').submit();">Limpiar</a>
    </li>


    <form id="limpiar-form" action="{{ url('reservas-limpiar') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>


  </ul>

</div>
</nav>

<main >
   @include('partial.flash')
 @yield('content')

</main><!-- /.container -->

    <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
      @yield('script')
    </body></html>