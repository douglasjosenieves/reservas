 @extends('layout.default')
 @section('title','Usuarios')
 @php
    $modulo = 'usuarios';
 @endphp
 {{-- expr --}}

 @section('content')
 

<div class="container">
 
  <div class="row justify-content-center">
    <div class="col-md-10">

<a href="{{ url('usuarios/create') }}" class="btn btn-success">Nuevo</a>
<hr> 

      <div class="card">
          

        <div class="card-header">@yield('title')</div>

        <div class="card-body">

          <div class="card-content">

           <br>
           <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" >
            <thead>
              <tr>
               <th>Id</th>

               <th>Name</th>
               <th>email</th>      
 
         
               <th>Creado</th>
               <th>Acción</th>



             </tr>

             <tbody>
               @foreach ($user as $element)


               <tr>
                <td>{{$element->id}} </td>
                <td>{{$element->name}} </td>
                <td>{{$element->email}} </td>             
                <td>{{$element->created_at}}</td>
                <td>@include('partial.action')</td>



              </tr>

              <!-- Classic Modal -->
              <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                      x
                      </button>
                      
                    </div>
                    <div class="modal-body">



                      {!! Form::open(['method' => 'PUT', 'url' => 'usuarios/'.$element->id, 'class' => 'form-horizontal']) !!}


                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', $element->name, ['class' => 'form-control', 'required' => 'required' ]) !!}
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>

                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('email', 'Email address') !!}
                        {!! Form::email('email', $element->email, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: foo@bar.com' ]) !!}
                        <small class="text-danger">{{ $errors->first('email') }}</small>
                      </div>
                     


              


                  
                    

                  </div>


                  <div class="modal-footer">

                    {!! Form::submit('Actualizar', ['class' => 'btn btn-warning pull-right']) !!}

                    {!! Form::close() !!}

                  </div>
                </div>
              </div>
            </div>
            <!--  End Modal -->
            @endforeach


          </tbody>
        </table>

        {{ $user->links() }}
      </div>

    </div>
  </div>
</div>
</div>
</div>
@endsection

@section('script')
{{-- expr --}}
@endsection