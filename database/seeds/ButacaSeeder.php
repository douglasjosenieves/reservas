<?php

use App\Butaca;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class ButacaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


    	for ($i = 1; $i <= Config::get('app.filas'); $i++) { 

    		for ($c = 1; $c <= Config::get('app.columnas'); $c++) { 

    			$butaca = new Butaca;
    			$butaca->code = $i.'-'.$c;
    			$butaca->save();

    		}


    	}





    }
}
